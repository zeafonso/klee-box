#!/bin/bash

if [ -z ${GUEST_USER} ] || \
   [ -z ${GUEST_GROUP} ] || \
   [ -z ${GUEST_SHARED_DIR} ]; then
    echo "${0##*/}: required Dockerfile env variables not defined:"
    echo "${0##*/}: GUEST_USER, GUEST_GROUP, GUEST_SHARED_DIR"
    exit 1
fi

if [ -z ${HOST_EUID} ] || \
   [ -z ${HOST_EGID} ]; then
    echo "${0##*/}: required command shell env variables not set:"
    echo "${0##*/}: HOST_EUID, HOST_EGID"
    exit 1
fi

# Make sure the guest user owns the shared (workspace) directory
chown ${GUEST_USER}:${GUEST_GROUP} ${GUEST_SHARED_DIR}

# Fix ID of guest user if required
USER_FIXED="no"
if [ ${HOST_EUID} -ne `id -u ${GUEST_USER}` ]; then
    usermod --uid ${HOST_EUID} ${GUEST_USER}
    USER_FIXED="yes"
fi

# Fix effective group ID of guest user if required
if [ ${HOST_EGID} -ne `id -g ${GUEST_USER}` ]; then
    groupmod --gid ${HOST_EGID} ${GUEST_GROUP}
    USER_FIXED="yes"
fi

# Update owner of home directory of guest user if needed
if [ ${USER_FIXED} == "yes" ]; then
    HOME_DIR="/home/${GUEST_USER}"
    chown -R ${GUEST_USER}:${GUEST_GROUP} ${HOME_DIR}
fi

# If needed, add host proxy environment variables
# to container's sudoers configuration
if [ -f /etc/sudoers ]; then
    SUDOERS_ENV_KEEP="Defaults env_keep"
    grep "^${SUDOERS_ENV_KEEP}=\"" /etc/sudoers | \
        grep -q \
             -e "http_proxy" -e "https_proxy" \
             -e "HTTP_PROXY" -e "HTTPS_PROXY" \
             -e "no_proxy" -e "NO_PROXY"
    if (($? != 0)); then
        PROXY_VARS=""
        if [ ! -z "$https_proxy" ]; then
            PROXY_VARS="https_proxy"
        elif [ ! -z "$HTTPS_PROXY" ]; then
            PROXY_VARS="HTTPS_PROXY"
        fi
        if [ ! -z "$http_proxy" ]; then
            PROXY_VARS="${PROXY_VARS} http_proxy"
        elif [ ! -z "$HTTP_PROXY" ]; then
            PROXY_VARS="${PROXY_VARS} HTTP_PROXY"
        fi
        if [ ! -z "$no_proxy" ]; then
            PROXY_VARS="${PROXY_VARS} no_proxy"
        elif [ ! -z "$NO_PROXY" ]; then
            PROXY_VARS="${PROXY_VARS} NO_PROXY"
        fi
        if [ ! -z "$PROXY_VARS" ]; then
            echo -e "\n${SUDOERS_ENV_KEEP}=\"${PROXY_VARS}\"" >> /etc/sudoers
        fi
    fi
fi

# Cleanly switch to guest user to execute start command
exec /usr/local/bin/gosu ${GUEST_USER}:${GUEST_GROUP} "$@"
