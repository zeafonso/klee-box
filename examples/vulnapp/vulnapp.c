/*
 * vulnapp.c
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <klee/klee.h>
#include "vulnapi.h"

int test(int test_case, char* input_string, int input_string_length) {
  int ret = 1;
  switch(test_case) {
    case 0:
      ret = echo_stack_overflow(input_string, input_string_length);
      break;
    case 1:
      ret = echo_int_overflow(input_string, input_string_length);
      break;
    case 2:
      ret = echo_heap_overflow(input_string, input_string_length);
      break;
    case 3:
      ret = echo_double_free(input_string, input_string_length);
      break;
    case 4:
      ret = echo_use_after_free(input_string, input_string_length);
      break;
    default:
      printf("Error: unexpected test case selection (%d)\n", test_case);
      break;
  }
  return(ret);
}

int main() {
  int test_case;
  char input_string[128];
  int input_string_length;
  int ret = 1;

  // Make the test case choice symbolic
  klee_make_symbolic(&test_case, sizeof(test_case), "test_case");
  klee_assume((0 <= test_case) & (test_case <= 4));

  // Make the input buffer contents symbolic
  klee_make_symbolic(input_string, sizeof(input_string), "input_string");

  // Make the input buffer length indication symbolic
  klee_make_symbolic(&input_string_length, sizeof(input_string_length),
    "input_string_length");

  ret = test(test_case, input_string, input_string_length);
  exit(ret);
}
