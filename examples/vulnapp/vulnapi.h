/*
 * vulnapi.h
 */
#ifndef _VULNAPI_
#define _VULNAPI_

int echo_stack_overflow(char* data, int data_len);
int echo_int_overflow(char* data, int data_len);
int echo_heap_overflow(char* data, int data_len);
int echo_double_free(char* data, int data_len);
int echo_use_after_free(char* data, int data_len);

#endif /* _VULNAPI_ */