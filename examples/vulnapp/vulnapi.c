/*
 * vulnapi.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "vulnapi.h"

#define BUFFER_SIZE 16

int echo_stack_overflow(char* data, int data_len) {
  char buffer[BUFFER_SIZE];

  printf("echo_stck_ovrflw: len=%d\n", data_len);

  // stack overflow if data_len >= sizeof(buffer)
  memcpy(buffer, data, data_len);

  return(0);
}

int echo_int_overflow(char* data, int data_len) {
  char buffer[BUFFER_SIZE];
  int len = sizeof(buffer);

  printf("echo_int_ovrflw: len=%d\n", data_len);

  // integer overflow if data_len < 0
  if (len > data_len) {
    len = data_len;
  }
  memcpy(buffer, data, len);

  return(0);
}

int echo_heap_overflow(char* data, int data_len) {
  char *buffer;

  printf("echo_hp_ovrflw: len=%d\n", data_len);

  buffer = malloc(BUFFER_SIZE);
  if (buffer == NULL) {
    printf("echo_hp_ovrflw: malloc() failed (%d)\n", BUFFER_SIZE);
    return(1);
  }

  // heap overflow if data_len >= BUFFER_SIZE
  memcpy(buffer, data, data_len);

  free(buffer);
  return(0);
}

int echo_double_free(char* data, int data_len) {
  char *buffer;

  printf("echo_dbl_free: len=%d\n", data_len);

  buffer = malloc(BUFFER_SIZE);
  if (buffer == NULL) {
    printf("echo_dbl_free: malloc() failed (%d)\n", BUFFER_SIZE);
    return(1);
  }

  memset(buffer, 0, BUFFER_SIZE);

  free(buffer);
  if ((data_len % 13) == 12) {
    // double free here if data_len % 13 == 12
    free(buffer);
  }

  return(0);
}

int echo_use_after_free(char* data, int data_len) {
  char* buffer;

  printf("echo_uaf_free: len=%d\n", data_len);

  buffer = malloc(BUFFER_SIZE);
  if (buffer == NULL) {
    printf("echo_uaf_free: malloc() failed (%d)\n", BUFFER_SIZE);
    return(1);
  }

  free(buffer);
  if ((data_len % 13) == 12) {
    // use after free here if data_len % 13 == 12
    memset(buffer, 0, BUFFER_SIZE);
  }

  return(0);
}
