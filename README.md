# klee-box/klee-box

This project can be used to build and launch a [Docker](https://www.docker.com/)
image which adds the following features to
[the official KLEE Docker image](https://klee.github.io/docker/):

1. Support testing your C/C++ projects with KLEE by
[building KLEE executables from the source code](https://klee.github.io/tutorials/testing-coreutils)
using Tristan Ravitch's
[Whole Program LLVM (WLLVM)](https://github.com/travitch/whole-program-llvm)
1. Support to build the KLEE image and execute it from a persistent Docker
container from a single script
1. Support to sharing of the same host machine by multiple users for
independent KLEE testing sessions
1. Automatic detection/use of HTTP proxy for Docker image/container setup

## Host machine requirements

 1. Install a 64-bit version of [Ubuntu](https://www.ubuntu.com/download) 14.04 or
 more recent
 1. Install [git](https://help.ubuntu.com/lts/serverguide/git.html#git-installation)
 1. Install [Docker](https://www.docker.com/docker-ubuntu)
 1. Make sure your user account in the host machine [has the the docker group](https://docs.docker.com/engine/installation/linux/linux-postinstall/#manage-docker-as-a-non-root-user)
 1. Install [Docker Compose](https://docs.docker.com/compose/install/)
 1. If your host machine is behind a HTTP proxy, please also make sure
 that its Docker daemon is [properly configured for that](https://docs.docker.com/engine/admin/systemd/#httphttps-proxy)

## Create and run your klee-box container

Execute in the host machine the following commands to build/start a `klee-box`
container:

```bash
git clone git@gitlab.com:zeafonso/klee-box.git
./klee-box/klee-box
```

- A `klee-box/klee-box` Docker image is automatically built if it does not exist
yet. This image will be used to create `klee-box` containers

- A persistent `klee-box` container named `klee-box-<your user name>` is also
automatically created for you if there isn't one yet.

- The credentials of your user account in the host machine are automatically
replicated (same uid, gid) by the `klee` user in the `klee-box` container

- The host directory from which `klee-box` script is executed is also
automatically mounted by the `klee-box` container at `/home/klee/workspace`.
It is shared by your user in the host machine and the `klee` user in the
container. Use it to store the source files of the project you wish to build
and execute using KLEE

- An interactive command shell prompt is started for you as the container's
`klee` user.

- Building KLEE executables from your project's source code and getting them
executed needs to be done from a shell of the `klee-box` container though

- Examples of project makefiles to build and run a project with KLEE from a
`klee-box` container can be found in `examples` directory

- It is more convenient to edit from the host machine the source code of your
project (stored in the shared `workspace` directory)

- Once you have exited the `klee-box` container shell, you can get back to it
anytime by running again the same script:

```bash
./klee-box/klee-box
```

- Your host machine can be shared with other users as a different instance of
the `klee-box` container is created for each user

## Running klee-box from behind an HTTP proxy

An HTTP proxy will be automatically detected and used when `klee-box` script
is executed in the host machine from a shell with environment variable
`http_proxy` (`https_proxy` or `no_proxy`) set.

## Additional useful information

 1. [KLEE website](https://klee.github.io/)
 1. [KLEE image in Docker Store](https://store.docker.com/community/images/klee/klee)
 1. [WLLVM repository](https://github.com/travitch/whole-program-llvm)