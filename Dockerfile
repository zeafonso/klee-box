FROM klee/klee:1.4.0
MAINTAINER Jose Pinto <zeafonso@gmail.com>

ENV GUEST_USER klee
ENV GUEST_GROUP klee
ENV GOSU_VERSION 1.10

USER root

RUN set -ex; \ 
	# Install gosu
	fetchDeps=' \
		ca-certificates \
	'; \
	apt-get update; \
	apt-get install -y --no-install-recommends $fetchDeps; \
	rm -rf /var/lib/apt/lists/*; \
	dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
	wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch"; \
	wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc"; \
	# Verify signature
	export GNUPGHOME="$(mktemp -d)"; \
	gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4; \
	gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu; \
	rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc; \
	chmod +x /usr/local/bin/gosu; \
	# Verify that gosu binary works
	gosu nobody true; \
	# Install WLLVM
    pip3 install --upgrade wllvm; \
    echo "export LLVM_COMPILER=clang" >> /home/klee/.bashrc

COPY ./docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["/bin/bash"]
